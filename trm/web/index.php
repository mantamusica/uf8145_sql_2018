<?php 
if (isset($_REQUEST['update'])){
    $mysqli = new mysqli("localhost", "root", "", "temperaturas");
    echo presentacion($mysqli);
    $mysqli->close();
    exit();
}
?>
<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Práctica 6</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/estilos.css">
  </head>
  <body>
    <div class="container" id="salida">
<?php
    $mysqli = new mysqli("localhost", "root", "", "temperaturas");
if (isset($_REQUEST['v']) && isset($_REQUEST['s'])){
    $mysqli->query('insert into arduinos(codigo,voltaje) values ('. $_REQUEST['s'] .', ' . $_REQUEST['v'] . ');');
}else{
    echo presentacion($mysqli);
}
    $mysqli->close();



function presentacion($mysqli){
    $actual = $mysqli->query('Select DISTINCT c1.codigo,c1.reciente,voltaje from (select codigo,MAX(fecha) reciente from arduinos group by codigo) c1 join arduinos on c1.reciente=fecha order by c1.codigo;');
    $resultMin = $mysqli->query('SELECT c1.codigo, reciente, truncate(min(voltaje),2) minimo FROM arduinos NATURAL JOIN (
  select codigo, date(MAX(fecha)) reciente from arduinos group by codigo) c1 GROUP BY codigo;');
    $resultMedia = $mysqli->query('SELECT c1.codigo, reciente, truncate(avg(voltaje),2) media FROM arduinos NATURAL JOIN (
  select codigo, date(MAX(fecha)) reciente from arduinos group by codigo) c1 GROUP BY codigo;');
    $resultMax = $mysqli->query('SELECT c1.codigo, reciente, truncate(MAX(voltaje),2) maximo FROM arduinos NATURAL JOIN (
  select codigo, date(MAX(fecha)) reciente from arduinos group by codigo) c1 GROUP BY codigo;');
    $resultCodigos = $mysqli->query('SELECT DISTINCT codigo FROM arduinos;');
    $datos = [];
    while ($fila = $resultCodigos->fetch_assoc()){
        $datos[$fila['codigo']] = [
            'maximo'=>0, 
            'minimo'=>0, 
            'media'=>0, 
            'actual' =>0, 
            'fecha'=>'',
            'obsoleto'=>false];
    }
    while ($fila = $resultMax->fetch_assoc()){
        $datos[$fila['codigo']]['maximo'] = $fila['maximo'];
    }
    while ($fila = $resultMin->fetch_assoc()){
        $datos[$fila['codigo']]['minimo'] = $fila['minimo'];
    }
    while ($fila = $resultMedia->fetch_assoc()){
        $datos[$fila['codigo']]['media'] = $fila['media'];
    }
    while ($fila = $actual->fetch_assoc()){
        $datos[$fila['codigo']]['actual'] = $fila['voltaje']*100;
        $datos[$fila['codigo']]['fecha'] = explode(" ", $fila['reciente'])[0];
        $datos[$fila['codigo']]['obsoleto'] = obsoleto($fila['reciente']);
    }
    $salida = '<h2 class="scale-transition center-align card-panel teal white-text">Gestor Temperaturas</h2>';
    $salida .= '<div class="row">';
    foreach ($datos as $codigo=>$datos) {
        $salida .= '<div class="col s4">';
        $salida .= '<div class="card-panel blue-grey darken-1 z-depth-3 white-text">';
        $salida .= '<h5 class="center-align">' . $codigo . '</h5>';
        $salida .= '<h3 class="center-align ';
        $salida .= ($datos['obsoleto']) ? 'grey-text">' : 'yellow-text">';
        $salida .= $datos['actual'] . '</h3>';
        $salida .= '<p class="center-align">' . $datos['fecha'] . '</p>';
        $salida .= '<p class="center-align ">' . $datos['maximo'] .'</p>';
        $salida .= '<p class="center-align ">' . $datos['media'] . '</p>';
        $salida .= '<p class="center-align ">' . $datos['minimo'] .'</p>';
        $salida .= '</div></div>';
    }
    $salida .= '</div>';
    return $salida;
}

function obsoleto($reciente){
    return (time() - (60*4) > strtotime($reciente));
}
?>
                                       
</div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>;
  </body>
</html>
