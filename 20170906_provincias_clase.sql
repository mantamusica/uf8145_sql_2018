USE aula;
CREATE TABLE provincias(
  autonomia varchar(127),
  provincia varchar(127)
  );
INSERT INTO `provincias` (`autonomia`, `provincia`) VALUES
('Galicia', 'La Coru�a'),
('Galicia', 'Lugo'),
('Galicia', 'Orense'),
('Galicia', 'Pontevedra'),
('Principado de Asturias', 'Asturias'),
('Cantabria', 'Cantabria'),
('Pa�s Vasco', 'Vizcaya'),
('Pa�s Vasco', 'Guip�zcoa'),
('Pa�s Vasco', '�lava'),
('La Rioja', 'La Rioja'),
('Comunidad Foral de Navarra', 'Navarra'),
('Arag�n', 'Huesca'),
('Arag�n', 'Zaragoza'),
('Arag�n', 'Teruel'),
('Catalu�a', 'Barcelona'),
('Catalu�a', 'Tarragona'),
('Catalu�a', 'L�rida'),
('Catalu�a', 'Gerona'),
('Comunidad Valenciana', 'Castell�n'),
('Comunidad Valenciana', 'Valencia'),
('Comunidad Valenciana', 'Alicante'),
('Regi�n de Murcia', 'Murcia'),
('Andaluc�a', 'Huelva'),
('Andaluc�a', 'Sevilla'),
('Andaluc�a', 'C�rdoba'),
('Andaluc�a', 'Ja�n'),
('Andaluc�a', 'Almer�a'),
('Andaluc�a', 'Granada'),
('Andaluc�a', 'M�laga'),
('Andaluc�a', 'C�diz'),
('Extremadura', 'C�rceres'),
('Extremadura', 'Badajoz'),
('Comunidad de Madrid', 'Madrid'),
('Castilla y Le�n', 'Le�n'),
('Castilla y Le�n', 'Zamora'),
('Castilla y Le�n', 'Salamanca'),
('Castilla y Le�n', 'Palencia'),
('Castilla y Le�n', 'Burgos'),
('Castilla y Le�n', 'Valladolid'),
('Castilla y Le�n', '�vila'),
('Castilla y Le�n', 'Segovia'),
('Castilla y Le�n', 'Soria'),
('Castilla-La Mancha', 'Toledo'),
('Castilla-La Mancha', 'Ciudad Real'),
('Castilla-La Mancha', 'Albacete'),
('Castilla-La Mancha', 'Cuenca'),
('Castilla-La Mancha', 'Guadalajara'),
('Canarias', 'Santa Cruz de Tenerife'),
('Canarias', 'Las Palmas'),
('Islas Baleares', 'Baleares');

/* CONSULTAS DE SELECCI�N */
-- Mostrar todos los campos y todos los registros de la tabla provincias
SELECT * FROM provincias;
-- Listado de provincias
SELECT provincia FROM provincias;
-- Listado de autonom�as
SELECT DISTINCT autonomia  FROM provincias;
-- provincias que empiezan por A
SELECT provincia FROM provincias 
  WHERE provincia LIKE 'A%';
-- provincias que contienen el diptongo 'ue'
SELECT provincia FROM provincias
  WHERE provincia LIKE '%ue%';
-- provincias con el mismo nombre que su
-- comunidad aut�noma
SELECT * FROM provincias 
  WHERE autonomia=provincia;
-- autonom�as que empiezan por C (8)
SELECT DISTINCT autonomia FROM provincias
  WHERE autonomia LIKE 'C%';
-- autonom�as que contienen la acepci�n 'Comunidad'
SELECT DISTINCT autonomia FROM provincias 
  WHERE autonomia LIKE '%comunidad%';
-- autonom�as terminadas en 'ana'
SELECT DISTINCT autonomia FROM provincias 
  WHERE autonomia LIKE '%ana';
-- autonom�as que comiencen por 'can'
SELECT DISTINCT autonomia FROM provincias
  WHERE autonomia LIKE 'can%';
-- �Qu� provincias tienen nombre compuesto?
SELECT DISTINCT provincia FROM provincias 
  WHERE provincia LIKE '% %';
-- �Qu� autonom�as tienen nombre compuesto?
SELECT DISTINCT autonomia FROM provincias 
  WHERE autonomia LIKE '% %';
-- �Qu� autonom�as tienen provincias con nombre compuesto?
SELECT DISTINCT autonomia FROM provincias 
  WHERE provincia LIKE '% %';
-- �Qu� provincias tienen autonom�as con nombre compuesto?
SELECT DISTINCT provincia FROM provincias 
  WHERE autonomia LIKE '% %';
-- �Qu� provincias tienen nombre simple?
SELECT DISTINCT provincia FROM provincias 
  WHERE provincia NOT LIKE '% %';
-- �Qu� autonom�as tienen nombre simple?
SELECT DISTINCT autonomia FROM provincias 
  WHERE autonomia NOT LIKE '% %';

/* CONSULTAS DE TOTALES */
-- �Cu�nto suman 2 y 3?
SELECT 2+3;
-- �Cu�nto vale la ra�z cuadrada de 2?
SELECT SQRT(2);
-- �Cu�ntas provincias hay?
SELECT COUNT(*) FROM provincias;
-- �Cu�ntas comunidades aut�nomas hay?
SELECT COUNT(DISTINCT autonomia) FROM provincias;
-- �Cu�ntos caracteres tiene cada nombre de provincia?
SELECT provincia,LENGTH(provincia) FROM provincias;
-- �Cu�ntos caracteres tiene cada nombre de comunidad aut�noma?
SELECT DISTINCT autonomia,LENGTH(autonomia)
  FROM provincias;
-- �Cu�nto mide el nombre de provincia m�s largo?
SELECT MAX(LENGTH(provincia)) FROM provincias;
-- �Cu�nto mide el nombre de provincia m�s corto?
SELECT MIN(LENGTH(provincia)) FROM provincias;
-- �Cu�nto mide el nombre de autonom�a m�s largo?
SELECT MAX(LENGTH(autonomia)) FROM provincias;
-- �Cu�l es el nombre de autonom�a m�s corto?
SELECT MIN(LENGTH(autonomia)) FROM provincias;
-- En un listado alfab�tico, �qu� provincia estar�a la primera?
SELECT MIN(provincia) FROM provincias;
-- En un listado alfab�tico, �qu� provincia estar�a la �ltima?
SELECT MAX(provincia) FROM  provincias;
-- �Qu� comunidades aut�nomas contienen el nombre de una de sus provincias?
SELECT autonomia FROM provincias
  WHERE LOCATE(provincia, autonomia) > 0;
-- �qu� provincias tienen el nombre m�s largo que el de su autonom�a?
SELECT provincia FROM provincias AS p
  WHERE LENGTH(provincia) > LENGTH(autonomia);


/* CONSULTAS DE PRESENTACI�N ��No hay �lgebra para la presentaci�n!! */
-- Presenta un listado de provincias ordenadas alfab�ticamente
SELECT DISTINCT provincia FROM provincias AS p ORDER BY p.provincia;
-- Presenta un listado de provincias ordenadas alfab�ticamente a la inversa
SELECT DISTINCT provincia FROM provincias AS p ORDER BY p.provincia DESC;
-- Presenta un listado de autonom�as ordenadas alfab�ticamente
SELECT DISTINCT autonomia FROM provincias AS p ORDER BY p.autonomia;
-- Presenta un listado de autonom�as ordenadas alfab�ticamente, 
-- con todas su provincias ordenadas alfab�ticamente en orden inverso
SELECT DISTINCT autonomia FROM provincias AS p ORDER BY p.autonomia, p.provincia
  desc;
-- Presenta un listado alfab�tico de provincias, en una �nica columna,
-- indicando entre par�ntesis la comunidad aut�noma a la que pertenece
SELECT CONCAT(p.provincia,' (',p.autonomia,')') FROM provincias AS p
  ORDER BY p.provincia;

/* CONSULTAS DE AGRUPAMIENTO */

-- �Cu�ntas provincias tiene cada comunidad aut�noma?
SELECT autonomia,COUNT(provincia) n FROM provincias AS p
  GROUP BY p.autonomia
  ORDER BY n DESC;

-- Listado de provincias por autonom�a ordenadas de m�s a menos

-- Listado del n�mero de provincias por autonom�a ordenadas de m�s a menos
-- y por orden alfab�tico si coinciden
SELECT autonomia,COUNT(provincia) n FROM provincias AS p
  GROUP BY p.autonomia
  ORDER BY n DESC, autonomia;
-- �Cu�ntas provincias con nombre compuesto tiene cada comunidad aut�noma?
SELECT p.autonomia, COUNT(p.provincia) FROM provincias AS p
  WHERE p.provincia LIKE '% %'
  GROUP BY p.autonomia;
-- �Cu�ntas provincias con nombre simple tiene cada comunidad aut�noma?
SELECT p.autonomia, COUNT(p.provincia) FROM provincias AS p
  WHERE p.provincia NOT LIKE '% %'
  GROUP BY p.autonomia;
-- Autonom�as uniprovinciales
SELECT autonomia FROM provincias AS p
  GROUP BY p.autonomia
  HAVING COUNT(provincia)=1;
-- Autonom�as uniprovinciales alfab�ticamente en orden inverso
SELECT autonomia FROM provincias AS p
  GROUP BY p.autonomia
  HAVING COUNT(provincia)=1
  ORDER BY p.autonomia DESC;

/* SUBCONSULTAS */
-- �Cu�l es el nombre de provincia m�s largo?
 SELECT provincia FROM provincias AS p
  WHERE LENGTH(p.provincia) = (
    SELECT MAX(LENGTH(provincia)) 
      FROM provincias);

-- �Cu�l es el nombre de provincia m�s corto?
   SELECT provincia FROM provincias AS p
  WHERE LENGTH(p.provincia) = (
    SELECT MIN(LENGTH(provincia)) 
      FROM provincias);

-- �Cu�l es el nombre de autonom�a m�s largo?
  SELECT autonomia FROM provincias
    WHERE LENGTH(provincias.autonomia) = (
      SELECT MAX(LENGTH(autonomia)) FROM provincias);
     
-- �Cu�l es el nombre de autonom�a m�s corto?
    SELECT autonomia FROM provincias
    WHERE LENGTH(provincias.autonomia) = (
      SELECT Min(LENGTH(autonomia)) FROM provincias);

-- �Cu�l es la autonom�a con m�s provincias?
SELECT provincias.autonomia, COUNT(*) FROM provincias
  GROUP BY autonomia;
  
SELECT MAX(num_prov) FROM (
    SELECT provincias.autonomia, COUNT(provincia) num_prov FROM provincias
    group BY provincias.autonomia
    ) c2;

-- consulta final <--
SELECT c1.autonomia FROM(
    SELECT provincias.autonomia, COUNT(provincia) num_prov FROM provincias
    group BY provincias.autonomia
    ) c1 WHERE c1.num_prov = (SELECT MAX(num_prov) FROM (
    SELECT provincias.autonomia, COUNT(provincia) num_prov FROM provincias
    group BY provincias.autonomia
    )c2 );


-- ////// Consultas de clase

-- �Qu� comunidades aut�nomas contienen el nombre de una de sus provincias?
SELECT autonomia FROM provincias
  WHERE LOCATE(provincia, autonomia) > 0;
SELECT autonomia FROM provincias
  WHERE provincias.autonomia LIKE CONCAT('%',provincia,'%');
-- �Podemos usar el alias para filtrar? �Y para ordenar?

-- �Qu� comunidades aut�nomas contienen el nombre de una provincia?
-- OJO: esta consulta ya no es registro a registro, debemos buscar datos en otros registros de la tabla
 SELECT DISTINCT(p2.autonomia), p1.provincia FROM provincias p1, provincias p2 
  WHERE LOCATE(p1.provincia,p2.autonomia) >0;

-- Listado de las 3 primeras letas de cada provincia en may�scula
SELECT UPPER(SUBSTR(provincia, 1,3)) FROM provincias;
-- �A qu� provincias corresponden cada una de las abreviaturas anteriores?
SELECT UPPER(SUBSTR(provincia, 1,3)), provincia FROM provincias;
-- �A cu�ntas provincias corresponde cada abreviatura?
SELECT COUNT(*), abr FROM (SELECT UPPER(SUBSTR(provincia, 1,3)) abr FROM provincias) c1 GROUP BY abr;
-- �Cu�ntas abreviatura de provincia son un�vocas?
SELECT COUNT(*) FROM (SELECT COUNT(*) FROM 
  (SELECT UPPER(SUBSTR(provincia, 1,3)) abr FROM provincias) c1 
  GROUP BY abr HAVING COUNT(*) = 1) c2;
-- �Cu�ntas provincias agrupar�an las 3 primeras letras de cada autonom�a?
-- Total de provincias cuyas autonom�as comparten sus tres primeras letras.
SELECT abr, COUNT(*) n FROM (
  SELECT SUBSTR(autonomia,1,3) abr from provincias) c1
  GROUP BY abr HAVING n>1;

-- �Cu�ntas autonom�as comparten las 3 primeras letras?
SELECT c1.abr, COUNT(*) n FROM (
SELECT DISTINCT provincias.autonomia, SUBSTR(autonomia,1,3) abr FROM provincias) c1 
  GROUP BY abr HAVING n>1;

SELECT sum(n) FROM (
SELECT c1.abr, COUNT(*) n FROM (
SELECT DISTINCT provincias.autonomia, SUBSTR(autonomia,1,3) abr FROM provincias) c1 
  GROUP BY abr HAVING n>1  
  )c2;

-- �Qu� autonom�as comparten las 3 primeras letras?





