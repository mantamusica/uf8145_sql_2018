-- 1. Genera un listado de los programas y cantidades que se han distribuido a El Corte Ingl�s de Madrid.
-- C1 = pi cif sigma nombre = 'El Corte Ingl�s' and ciudad = 'Madrid' (comercio)
-- R1 = pi nombre, version, cantidad (C1 natural join distribuye natural join programa)
SELECT nombre, VERSION, cantidad 
  FROM (SELECT cif FROM comercio WHERE nombre LIKE 'El Corte Ingl�s' AND ciudad LIKE 'Madrid')c1
  NATURAL JOIN distribuye NATURAL JOIN programa;

-- 2. Nombre de aquellos fabricantes cuyo pa�s es el mismo que Oracle
-- C2 = pi pais sigma nombre = 'Oracle' (fabricante)
-- R2 = pi nombre (C2 natural join fabricante)
SELECT nombre FROM (fabricante) 
  WHERE fabricante.pais = (SELECT pais FROM fabricante WHERE fabricante.nombre = 'Oracle');

-- 3. Genera un listado en el que aparezca cada cliente junto al programa que ha registrado
-- , el medio con el que lo ha hecho y el comercio en el que lo ha adquirido.
-- R3 = pi nombre_cliente, programa.nombre, registra.medio, vendedor 
--      (rho nombre_cliente<-nombre (cliente) natural join registra natural join programa natural join (rho vendedor?nombre (comercio)))
SELECT cliente, nombre aplicacion, c3.medio, comercio
  FROM programa p JOIN(
    SELECT nombre cliente, c1.codigo, c1.medio, comercio
      FROM cliente c JOIN (
       SELECT dni, codigo,medio,nombre comercio
        FROM registra JOIN comercio USING(cif)
        ) c1 USING(dni) 
  ) c3 USING(codigo);

-- 4. Nombre de aquellos clientes que tienen la misma edad que Pepe P�rez
-- C4 = pi cliente.edad sigma nombre = 'Pepe P�rez' (cliente)
-- R4 = pi cliente.nombre (C4 natural join cliente)
SELECT nombre FROM (
  SELECT edad FROM cliente WHERE cliente.nombre = 'Pepe P�rez'
  )c1 NATURAL JOIN cliente;

-- 5. Con una consulta concatena los campos nombre y versi�n de la tabla PROGRAMA.
SELECT CONCAT(nombre, ' ' , version) FROM (programa);

-- 6. Nombre de aquellos clientes que han registrado un producto de la misma forma que el cliente Pepe P�rez. 
-- C6 = pi medio sigma nombre = 'Pepe P�rez' (registra natural join cliente)
-- R6 = pi nombre (C6 natural join registra natural join cliente)
SELECT nombre FROM (
  SELECT medio FROM registra NATURAL JOIN cliente WHERE nombre LIKE 'Pepe P�rez'
  ) c1 NATURAL JOIN registra NATURAL JOIN cliente;

-- 7.  Calcular el n�mero de productos que se han vendido de cada fabricante
-- R7 = gamma nombre; count(nombre) -> cuantos (registra natural join desarrolla natural join fabricante)
SELECT fabricante.nombre, COUNT(*) cuantos
  FROM registra
  NATURAL JOIN desarrolla natural JOIN fabricante GROUP BY nombre;

-- 8. Qu� comercio no distribuye ning�n programa desarrollado por Dinamic
-- C8 = pi id_fab sigma nombre = 'Dinamic' (fabricante)
-- C81 = pi codigo (desarrolla natural join C8)
-- C82 = pi cif (C81 natural join distribuye)
-- C83 = pi cif comercio
-- C84 = (C83 - C82) natural join comercio
-- pi nombre, ciudad C84
SELECT DISTINCT nombre, ciudad FROM (
  SELECT c4.cif FROM (
    SELECT cif FROM comercio
  ) c4 LEFT JOIN (
    SELECT DISTINCT cif FROM(
      SELECT codigo FROM desarrolla WHERE desarrolla.id_fab = (
        select id_fab FROM fabricante WHERE nombre ='Dinamic'
       )
     ) c2 JOIN distribuye USING(codigo)
  ) c3 ON c4.cif=c3.cif WHERE c3.cif IS NULL 
  ) c5 JOIN comercio USING(cif);
-- 9. Cada distribuidor, cu�ntos productos distribuye de cada fabricante.
-- R9 = gamma vendedor, ciudad, nombre; count(id_fab) -> n_productos 
--      ((rho vendedor<-nombre (comercio)) natural join distribuye natural join desarrolla join fabricante)
SELECT DISTINCT c.nombre, c.ciudad,f.nombre,np FROM(
  SELECT cif,id_fab,COUNT(DISTINCT codigo) np FROM 
    distribuye JOIN desarrolla USING(codigo) GROUP BY cif,desarrolla.id_fab
    ) c1 JOIN comercio c JOIN fabricante f
  ON c.cif=c1.cif AND f.id_fab = c1.id_fab ORDER BY c.nombre,c.ciudad,f.nombre;