USE aula;
CREATE TABLE provincias(
  autonomia varchar(127),
  provincia varchar(127)
  );
INSERT INTO `provincias` (`autonomia`, `provincia`) VALUES
('Galicia', 'La Coru�a'),
('Galicia', 'Lugo'),
('Galicia', 'Orense'),
('Galicia', 'Pontevedra'),
('Principado de Asturias', 'Asturias'),
('Cantabria', 'Cantabria'),
('Pa�s Vasco', 'Vizcaya'),
('Pa�s Vasco', 'Guip�zcoa'),
('Pa�s Vasco', '�lava'),
('La Rioja', 'La Rioja'),
('Comunidad Foral de Navarra', 'Navarra'),
('Arag�n', 'Huesca'),
('Arag�n', 'Zaragoza'),
('Arag�n', 'Teruel'),
('Catalu�a', 'Barcelona'),
('Catalu�a', 'Tarragona'),
('Catalu�a', 'L�rida'),
('Catalu�a', 'Gerona'),
('Comunidad Valenciana', 'Castell�n'),
('Comunidad Valenciana', 'Valencia'),
('Comunidad Valenciana', 'Alicante'),
('Regi�n de Murcia', 'Murcia'),
('Andaluc�a', 'Huelva'),
('Andaluc�a', 'Sevilla'),
('Andaluc�a', 'C�rdoba'),
('Andaluc�a', 'Ja�n'),
('Andaluc�a', 'Almer�a'),
('Andaluc�a', 'Granada'),
('Andaluc�a', 'M�laga'),
('Andaluc�a', 'C�diz'),
('Extremadura', 'C�rceres'),
('Extremadura', 'Badajoz'),
('Comunidad de Madrid', 'Madrid'),
('Castilla y Le�n', 'Le�n'),
('Castilla y Le�n', 'Zamora'),
('Castilla y Le�n', 'Salamanca'),
('Castilla y Le�n', 'Palencia'),
('Castilla y Le�n', 'Burgos'),
('Castilla y Le�n', 'Valladolid'),
('Castilla y Le�n', '�vila'),
('Castilla y Le�n', 'Segovia'),
('Castilla y Le�n', 'Soria'),
('Castilla-La Mancha', 'Toledo'),
('Castilla-La Mancha', 'Ciudad Real'),
('Castilla-La Mancha', 'Albacete'),
('Castilla-La Mancha', 'Cuenca'),
('Castilla-La Mancha', 'Guadalajara'),
('Canarias', 'Santa Cruz de Tenerife'),
('Canarias', 'Las Palmas'),
('Islas Baleares', 'Baleares');

/* CONSULTAS DE SELECCI�N */
-- Listado de provincias
SELECT provincia FROM provincias;
-- Listado de autonom�as
SELECT DISTINCT autonomia FROM provincias;
-- provincias que empiezan por A (6)
SELECT provincia FROM provincias 
  WHERE provincia LIKE 'a%';
-- provincias que contienen el diptongo 'ue'
select provincia FROM provincias 
  WHERE provincias.provincia LIKE '%ue%';
-- provincias con el mismo nombre que su comunidad aut�noma
SELECT provincia FROM provincias
  WHERE provincia LIKE autonomia; 
-- autonom�as que empiezan por C (8)
SELECT distinct autonomia FROM provincias
  WHERE autonomia LIKE 'C%';
-- autonom�as que contienen la acepci�n 'Comunidad'
SELECT DISTINCT autonomia FROM provincias
  WHERE autonomia LIKE '%Comunidad%';
-- autonom�as terminadas en 'ana'
SELECT DISTINCT autonomia FROM provincias AS p
  WHERE autonomia LIKE '%ana';
-- autonom�as que comiencen por 'can'
SELECT DISTINCT autonomia FROM provincias AS p
  WHERE autonomia LIKE 'can%';
-- �Qu� provincias tienen nombre compuesto?
SELECT DISTINCT provincia FROM provincias AS p
  WHERE provincia LIKE '% %';
-- �Qu� autonom�as tienen nombre compuesto?
SELECT DISTINCT autonomia FROM provincias AS p
  WHERE autonomia LIKE '% %';
-- �Qu� provincias tienen nombre simple?
SELECT DISTINCT provincia FROM provincias AS p
  WHERE provincia NOT LIKE '% %';
-- �Qu� autonom�as tienen nombre simple?
SELECT DISTINCT autonomia FROM provincias AS p
  WHERE autonomia NOT LIKE '% %';

/* CONSULTAS DE TOTALES */
-- �Cu�nto suman 2 y 3?
SELECT 2+3;
-- �Cu�nto vale la ra�z cuadrada de 2?
SELECT SQRT(2);
-- �Cu�ntas provincias hay?
SELECT COUNT(*) FROM provincias;
-- �Cu�ntas comunidades aut�nomas hay?
SELECT COUNT(DISTINCT autonomia) FROM provincias AS p;
-- �Cu�ntos caracteres tiene cada nombre de provincia?
SELECT LENGTH(provincia), provincia FROM provincias AS p;
-- �Cu�ntos caracteres tiene cada nombre de comunidad aut�noma?
SELECT DISTINCT LENGTH(autonomia), autonomia FROM provincias;
-- �Cu�ntos caracteres tiene el nombre de provincia m�s largo?
SELECT MAX(LENGTH(provincia)) FROM provincias;
-- �Cu�nto mide el nombre de provincia m�s corto?
SELECT MIN(LENGTH(provincia)) FROM provincias AS p;
-- �Cu�nto mide el nombre de autonom�a m�s largo?
SELECT MAX(LENGTH(autonomia)) FROM provincias;
-- �Cu�nto mide el nombre de autonom�a m�s corto?
SELECT MIN(LENGTH(autonomia)) FROM provincias AS p;
-- En un listado alfab�tico, �qu� provincia estar�a la primera?
SELECT provincia FROM provincias AS p ORDER BY provincia ASC LIMIT 1;
-- En un listado alfab�tico, �qu� provincia estar�a la �ltima?
SELECT provincia FROM provincias AS p ORDER BY provincia DESC LIMIT 1;
-- �Qu� provincias tiene un nombre m�s largo que el de su autonom�a?
SELECT provincias.provincia  FROM provincias
  WHERE LENGTH(provincias.provincia) > LENGTH(provincias.autonomia);

/* CONSULTAS DE PRESENTACI�N ��No hay �lgebra para la presentaci�n!! */
-- Presenta un listado de provincias ordenadas alfab�ticamente
  SELECT provincia FROM provincias
    ORDER BY provincias.provincia;

-- Presenta un listado de provincias ordenadas alfab�ticamente a la inversa
  SELECT provincia FROM provincias
    ORDER BY provincias.provincia desc;
-- Presenta un listado de autonom�as ordenadas alfab�ticamente
SELECT autonomia FROM provincias GROUP BY provincias.autonomia ORDER BY provincias.autonomia ASC;
-- Presenta un listado de autonom�as ordenadas alfab�ticamente, 
-- con todas su provincias ordenadas alfab�ticamente en orden inverso
SELECT provincias.autonomia, provincias.provincia FROM provincias
  GROUP BY provincias.autonomia ASC, provincias.provincia DESC;
-- Presenta un listado alfab�tico de provincias, en una �nica columna,
-- indicando entre par�ntesis la comunidad aut�noma a la que pertenece
SELECT CONCAT(provincias.provincia,'(',provincias.autonomia,')') FROM provincias;


/* CONSULTAS DE AGRUPAMIENTO */
-- �Cu�ntas provincias tiene cada comunidad aut�noma?
SELECT autonomia, COUNT(provincia) FROM provincias
  GROUP BY provincias.autonomia;
-- Listado de provincias por autonom�a ordenadas de m�s a menos
SELECT autonomia, COUNT(provincia) numero FROM provincias
  GROUP BY provincias.autonomia 
  ORDER BY numero desc;
-- Listado de provincias por autonom�a ordenadas de m�s a menos
-- y por orden alfab�tico si coinciden
SELECT autonomia, COUNT(provincia) numero FROM provincias
  GROUP BY provincias.autonomia 
  ORDER BY numero desc, autonomia;
-- �Cu�ntas provincias con nombre compuesto tiene cada comunidad aut�noma?
SELECT autonomia, COUNT(provincia) FROM provincias
  WHERE provincias.provincia LIKE '% %' GROUP  BY autonomia;
-- �Cu�ntas provincias con nombre simple tiene cada comunidad aut�noma?
SELECT autonomia, COUNT(provincia) FROM provincias
  WHERE provincias.provincia NOT LIKE '% %'
  GROUP BY provincias.autonomia;
-- Autonom�as uniprovinciales
SELECT autonomia FROM provincias
  GROUP BY provincias.autonomia
  HAVING COUNT(provincia) = 1;

-- Autonom�as uniprovinciales alfab�ticamente en orden inverso
SELECT autonomia FROM provincias
  GROUP BY provincias.autonomia
  HAVING COUNT(provincias.provincia) = 1
  ORDER BY provincias.autonomia DESC;

/* SUBCONSULTAS */
-- �Cu�l es la autonom�a con m�s provincias?
  -- c1 <--
  SELECT provincias.autonomia, COUNT(provincia) num_prov FROM provincias
    group BY provincias.autonomia;
  -- c2 <--
  SELECT MAX(num_prov) FROM (
    SELECT provincias.autonomia, COUNT(provincia) num_prov FROM provincias
    group BY provincias.autonomia
    ) c2;
-- consulta final <--
  SELECT c1.autonomia FROM(
    SELECT provincias.autonomia, COUNT(provincia) num_prov FROM provincias
    group BY provincias.autonomia
    ) c1 WHERE c1.num_prov = (SELECT MAX(num_prov) FROM (
    SELECT provincias.autonomia, COUNT(provincia) num_prov FROM provincias
    group BY provincias.autonomia
    )c2 ); 

-- �Cu�l es el nombre de provincia m�s largo?
  -- c1 <--
  SELECT MAX(LENGTH(provincia)) FROM provincias;
  -- Consulta final <--
  SELECT provincia FROM provincias
    WHERE LENGTH(provincias.provincia) = (
    SELECT MAX(LENGTH(provincia)) FROM provincias
      );
