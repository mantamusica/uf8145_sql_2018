<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Base de datos</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <?php include_once "libreria.php"; ?>
	<style type="text/css">
		body{
			text-align: center;
			font-family:Roboto,Open Sans,Verdana,Courier New;
		}
		form{
			margin: 20px auto;
		}
		textarea{
			height: 200px;
			width: 600px;
		}
	</style>
<script charset="utf-8" type="text/javascript">
window.addEventListener("load", function(){
    var lenguajeSelect = document.querySelector("#lenguajeSelect");
    lenguajes.forEach(function(obj){
        var opcion = document.createElement("option");
        opcion.text=obj.nombre;
        opcion.value=obj.id_lenguaje;
    lenguajeSelect.options.add(opcion);
});
    var alumnoSelect = document.querySelector("#alumnoSelect");
    alumnos.forEach(function(obj){
        var opcion = document.createElement("option");
        opcion.text=obj.nombre;
        opcion.value=obj.id_alumno;
    alumnoSelect.options.add(opcion);
});
    var nivelSelect = document.querySelector("#nivelSelect");
    niveles.forEach(function(obj){
        var opcion = document.createElement("option");
        opcion.text=obj.nivel;
        opcion.value=obj.id_nivel;
    nivelSelect.options.add(opcion);
});
document.querySelector("#relacional").addEventListener("submit", function(evt){
    evt.preventDefault();

});


})
</script>
</head>
<body>

	<h2>Tabla de Alumnos</h2>
	<form action="http://localhost/evento/registrador.php" method="post" target="_blank">
		<input type="text" 		name="nombre"		placeholder="Nombre" autofocus>
		<input type="text" 		name="apellidos"	placeholder="Apellidos">
		<input type="text" 		name="email"		placeholder="email">
		
		<input type="hidden" 	name="basedatos"	value="evento">
		<input type="hidden" 	name="tabla" 		value="alumnos">
		<button>Registrar</button>
	</form>
	
    <h2>Tabla de Lenguajes de programación</h2>
	<form action="http://localhost/evento/registrador.php" method="post" target="_blank">
		<input type="text" 		name="nombre"		placeholder="Lenguaje" autofocus>
		
		<input type="hidden" 	name="basedatos"	value="evento">
		<input type="hidden" 	name="tabla" 		value="lenguajes">
		<button>Registrar</button>
	</form>

	<h2>Relación entre alumnos y lenguajes de programación</h2>
	<form action="http://localhost/evento/registrador.php" method="post" target="_blank" id="relacional">

        <select name="id_alumno" id="alumnoSelect">      
        </select>
        <select name="id_lenguaje" id="lenguajeSelect">      
        </select>
        <select name="id_nivel" id="nivelSelect">      
        </select>
		<input type="number" min=0 max=100	name="experiencia"	placeholder="experiencia" >

		<input type="hidden" 	name="basedatos"	value="evento">
		<input type="hidden" 	name="tabla" 		value="conocen">

		<input type="hidden" 	name="tabla1" 		value="alumnos">
		<input type="hidden" 	name="id1" 			value="id_alumno">
		<input type="hidden" 	name="campo1" 		value="nombre">

		<input type="hidden" 	name="tabla2" 		value="lenguajes">
		<input type="hidden" 	name="id2" 			value="id_aficion">
		<input type="hidden" 	name="campo2" 		value="nombre">
        
        <input type="hidden" 	name="tabla3" 		value="niveles">
		<input type="hidden" 	name="id3" 			value="id_nivel">
		<input type="hidden" 	name="campo3" 		value="nivel">
        
        <input type="hidden" name="experiencia"/>

		<button>Registrar</button>
	</form>

</body>
</html>
