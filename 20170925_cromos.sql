USE aula;
/* 
- La colecci�n consta de 80 cromos y 20 pegatinas nombradas empezando por S
- Consideramos cromos tambi�n a las pegatinas
*/

/* CONSULTAS DE PROYECCI�N */
-- �Cu�les son los nombre de los ni�os que intercambian cromos?
SELECT DISTINCT interesado FROM cromos c;
-- Listado de cromos de cada ni�o sin repetidos
SELECT DISTINCT cromo,interesado FROM cromos c;
-- �Qu� cromos de la colecci�n tiene cada ni�o?
SELECT cromo,interesado FROM cromos c;
-- Entre todos los ni�os, �tenemos todos los cromos de la colecci�n?
SELECT DISTINCT cromo FROM cromos c ORDER BY cromo;
-- Cromos diferentes de Miguel
SELECT DISTINCT cromo FROM cromos 
  WHERE cromos.interesado = 'miguel';

/* CONSULTAS DE SELECCI�N */
-- �Qu� pegatinas han conseguido los ni�os?
SELECT cromo FROM cromos c
  WHERE c.cromo LIKE 's%';
-- �Qu� pegatinas diferentes han conseguido los ni�os?
SELECT DISTINCT  cromo FROM cromos c
  WHERE c.cromo LIKE 's%';

-- �Qu� ni�os tienen las pegatinas s19 y s20?
SELECT * FROM cromos c WHERE c.cromo='s19' OR c.cromo='s20';

/* CONSULTAS DE TOTALES */
-- �Cu�ntos ni�os intercambian cromos?
SELECT count( DISTINCT interesado) FROM cromos; 
-- �Cu�ntos cromos tiene Roxana?
SELECT COUNT(*) FROM cromos WHERE interesado = 'roxana';
-- �cu�ntos cromos diferentes tiene roxana?
SELECT COUNT(distinct cromo) FROM cromos WHERE interesado = 'roxana';
-- �cu�ntos repetidos?
SELECT (
  SELECT COUNT(*) FROM cromos WHERE interesado = 'roxana'
  )-(
  SELECT COUNT(distinct cromo) FROM cromos WHERE interesado = 'roxana'
  )repetidos;
-- �Cu�ntos cromos de la colecci�n nos faltan?
SELECT (
  SELECT COUNT(*) FROM cromos_album
  )-(
  SELECT COUNT(DISTINCT cromo) FROM cromos
  ) faltan;
-- �Cu�ntas pegatinas han conseguido los ni�os?
SELECT COUNT(cromo) FROM cromos WHERE cromo LIKE 's%';
-- �Cu�ntas pegatinas diferentes han conseguido los ni�os?
SELECT COUNT(distinct cromo) FROM cromos WHERE cromo LIKE 's%';

/* SUBCONSULTAS */
-- A la vista de estos datos, �cu�les ser�an los cromos m�s dif�ciles de conseguir?
SELECT * FROM cromos_album WHERE cromo NOT IN (SELECT DISTINCT  cromo FROM cromos);

/* CONSULTAS DE AGRUPAMIENTO */
-- �Cu�les son los cromos que m�s se repiten?

-- �Cu�ntos cromos tiene cada ni�o?

-- �Cu�ntos cromos diferentes tiene cada ni�o?

-- �Qu� cromos salen s�lo una vez?

-- �Qu� cromos hay repetidos?

-- listado de los cromos repetidos ordendos por n�mero de cromo

-- Cromos repetidos de Adri

-- �Qu� cromos y cu�ntas veces tiene Roxana cada cromo repetido?

-- �Qu� cromos tiene repetidos Roxana y cu�ntos de cada uno?


/* CONSULTAS DE AGRUPAMIENTO CON SUBCONSULTAS */
-- �Qu� cromos tienen m�s ni�os?

-- �Qu� cromos tienen todos los ni�os?

-- �Cu�ntos cromos tienen todos los ni�os en com�n (simult�neamente)?

-- �Cu�l es el cromo m�s repetido?

-- �Cu�les son los cromos m�s f�ciles?

-- Qu� cromos tiene repetidos Adri que le pueden interesar a Miguel?
-- Cromos repetidos de Adri
-- Cromos diferentes de Miguel
-- Resultado

-- �Qu� cromos tiene repetidos cada ni�o?

-- �Cu�ntos cromos repetidos diferentes tiene cada ni�o?

-- �Cu�ntos cromos repetidos, aunque sean iguales, tiene cada ni�o?

-- �Qu� cromos tiene Roxana que no tiene Adri?

-- Roxana quiere saber si le falta alguno de los repes de Adri

-- Miguel quiere saber cu�ntos cromos repetidos tienen Adri y Roxana y �l a�n no tiene
-- Cromos repetidos de Adri
-- Cromos repetidos de Roxana
-- Cromos de Miguel
-- Cromos repetidos de Adri que le interean a Miguel


/* PROCEDIMIENTOS ALMACENADOS */
-- Crear un procedimiento almacenado que cumplimente el album

-- Realiza un procedimiento almacenado para transformar los cromos que no tenga un ni�o en los que tiene