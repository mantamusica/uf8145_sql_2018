-- 1. Obtener los datos de las etapas que pasan por alg�n puerto de monta�a y tienen salida y llegada en la misma poblaci�n
-- C1 = pi numetapa sigma etapa.salida = etapa.llegada etapa
-- R1 = pi numetapa (C1 natural join puerto)

  SELECT DISTINCT c1.numetapa FROM (
    SELECT numetapa FROM etapa WHERE etapa.salida = etapa.llegada
    ) c1 JOIN puerto USING(numetapa);

-- 2. Obtener las poblaciones que tienen la meta de alguna etapa pero desde las que no se realiza ninguna salida
-- C2 = pi llegada etapa
-- C21 = pi salida etapa
-- R2 = C2 - C21
  SELECT * FROM (
    SELECT DISTINCT llegada FROM etapa
    ) c1 WHERE c1.llegada NOT IN (
    SELECT DISTINCT salida FROM etapa
    );

-- 3. Obtener el nombre y el equipo de los ciclistas que han ganado alguna etapa 
--    llevando el maillot amarillo, mostrando tambi�n el n�mero de etapa
-- nombre, nomequipo, dorsal y numero de etapa de ciclista que ha ganado etapa C1
-- C1 = pi nombre, nomequipo, dorsal, numetapa (ciclista natural join etapa)
-- de C1 seleccionar los que ganaron con el maillot amarillo
-- C11 = pi dorsal, numetapa sigma maillot.color = 'amarillo' (maillot natural join lleva)
-- R1 = pi nombre, nomequipo, numetapa (C11 natural join C1)
SELECT nombre, nomequipo, numetapa FROM (
  SELECT DISTINCT dorsal, numetapa FROM maillot NATURAL JOIN lleva WHERE maillot.color = 'amarillo'
  )c1 NATURAL JOIN (
  SELECT nombre, nomequipo, dorsal, numetapa  FROM ciclista NATURAL JOIN etapa
  )c2 ;

-- 4. Obtener los datos de las etapas que no comiencen en la misma ciudad en que acaba la etapa anterior
SELECT * FROM etapa c1
  WHERE c1.salida NOT IN (
    SELECT c2.llegada FROM etapa c2 WHERE c2.numetapa = (c1.numetapa -1));

-- 5.  Obtener los datos de los ciclistas que han vestido todos los maillots (no necesariamente en la misma etapa)
-- C5 = gamma count(color) -> maillots (maillot)
-- C51 = pi nombre, color (ciclista natural join lleva natural join maillot)
-- C52 = gamma nombre; count(color)->n_color C51
-- C53 = pi nombre sigma n_color = 6 C52
-- R5 = ciclista natural join C53
SELECT * FROM ciclista WHERE ciclista.nombre = (
  SELECT nombre FROM (
  SELECT nombre, COUNT(nombre) n_maillots FROM (
    SELECT DISTINCT nombre, color FROM ciclista NATURAL JOIN lleva NATURAL JOIN maillot
    )c1 GROUP BY nombre
  )c2 WHERE n_maillots = (SELECT COUNT(*) FROM maillot)
  );
-- solucion de David aunque aqu� no se obtienen todos los datos de los ciclistas
SELECT dorsal, COUNT(DISTINCT c�digo) maillots FROM lleva 
  GROUP BY lleva.dorsal having maillots=(SELECT COUNT(*) c1 FROM maillot);


-- 6. Obtener el c�digo y el color de aquellos maillots que s�lo han sido llevados por ciclistas de un mismo equipo
-- C6 = pi codigo, nomequipo (lleva natural join ciclista)
-- C61 = gamma codigo; count(*)->num C6
-- C62 = pi codigo sigma num = 1 C61
-- R6 = pi codigo, color (C62 natural join maillot)
SELECT c�digo, color FROM (
  SELECT c�digo FROM (
    SELECT c�digo, count(*) num FROM (
      SELECT DISTINCT c�digo, nomequipo FROM lleva NATURAL JOIN ciclista
    )c1 GROUP BY c�digo HAVING num=1
  )c2
  )c3 NATURAL JOIN maillot;


-- 7. �Cu�ntos ciclistas hay en cada equipo?
-- R7 = gamma nomequipo; count(nombre)->ciclistas (ciclista)
SELECT nomequipo, COUNT(*) FROM ciclista GROUP BY ciclista.nomequipo;

-- 8. �Cu�ntos ciclistas de m�s de 25 a�os hay en cada equipo?
-- C8 = sigma edad > 25 ciclista
-- R8 = gamma nomequipo; count(*) -> ciclistas C8
SELECT nomequipo, COUNT(*) ciclistas FROM ciclista
  WHERE ciclista.edad > 25 
  GROUP BY ciclista.nomequipo
  ORDER BY ciclistas DESC;

-- 9. �Qu� equipos tienen m�s de 7 corredores de m�s de 25 a�os?

-- 10. �Cu�ntos equipos tienen m�s de 7 corredores de m�s de 25 a�os?