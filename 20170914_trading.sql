-- //////////// markets
-- Listado completo de la tabla markets
SELECT * FROM markets;
-- Campos de la tabla markets
SHOW COLUMNS FROM markets;
EXPLAIN markets;
-- Listado de los nombres de mercados disponibles
SELECT DISTINCT marketname FROM markets;
-- Listado de las monedas negociadas en cada mercado con su abreviatura entre par�ntesis
SELECT CONCAT(m.MarketCurrencyLong,' (',m.MarketCurrency,')') FROM markets AS m;
-- Cantidad de mercados disponibles
SELECT COUNT(*) FROM markets AS m
  WHERE m.IsActive = 1;
-- Listado de los 5 mercados m�s longevos
SELECT * FROM markets AS m 
  ORDER BY m.Created        
  LIMIT 5;
-- Listado de los 5 mercados m�s novedosos
SELECT * FROM markets AS m 
  ORDER BY m.Created DESC       
  LIMIT 5;
-- Listado de los mercados m�s longevos
SELECT * FROM markets AS m
  WHERE m.Created = (SELECT MIN(m.Created) FROM markets AS m);
-- Listado de los mercados m�s novedosos sin tener en ceunta la hora;
SELECT * FROM markets WHERE DATE(Created) = (
    SELECT MAX(DATE(Created)) FROM markets);
-- Listado de los mercados m�s novedosos
SELECT * FROM markets AS m 
  ORDER BY m.Created DESC;
-- Listado de mercados inactivos

-- �Cu�ntos mercados se negocian con cada moneda base?
SELECT markets.BaseCurrencyLong, COUNT(*) FROM markets GROUP BY BaseCurrencyLong;
-- �Cu�ntos casos hay en los que no se respeta el CUR-CUR?
SELECT COUNT(*) FROM markets 
  WHERE markets.MarketName NOT LIKE '___-___';

-- ////////// currencies
-- Visualizar la tabla currencies completa
SELECT * FROM currencies;
-- Campos de la tabla currencies
EXPLAIN currencies;
-- Listado total de monedas negociadas en la casa de cambios
SELECT currencies.CurrencyLong FROM currencies;
-- detectar un duplicado en currencylong
SELECT COUNT(*) n,currencylong FROM currencies 
  GROUP BY currencies.CurrencyLong
  HAVING n > 1;
-- Listado de monedas inactivas

-- Cantidad de tipos de moneda

-- ///////////// currencies y markets
-- �Cu�ntas monedas y mercados hay?

-- �Cu�ntas monedas no est�n activas?

-- �Qu� monedas no est�n en los mercados?

-- ///// marketsummary
-- Tabla marketsummary completa

-- �Qu� monedas tiene m�s �rdenes de compra que de venta?

-- �Qu� monedas tienen m�s �rdenes de compra?

-- �Cu�l es el cambio USDT-BTC?

-- �Cu�l es el cambio USDT-ETH?

-- �Qu� mercados tienen mayor volumen en USDT

-- �Cu�les son las monedas que superan los 25 millones de USDT?

-- �Cu�l es el nombre completo de esas monedas?